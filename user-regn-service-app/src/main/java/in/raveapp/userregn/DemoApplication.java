package in.raveapp.userregn;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@EnableDiscoveryClient
@EnableAutoConfiguration
public class DemoApplication {

  @RequestMapping("/hi1")
  public String hello() {
    return "hello world!";
  }
}