package in.raveapp.userregn;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@SpringBootApplication
@EnableEurekaClient
public class UserRegnServiceAppInitializer {

	public static void main(String[] args) {
		SpringApplication.run(UserRegnServiceAppInitializer.class, args);
	}
}
